#!/bin/bash

# Mostly just https://github.com/curl/curl/blob/master/docs/HTTP3.md
# with the specific requirements for debian/kali

# Remark: change lib with lib64 for QUICTLS if you have a 64 bits kernel

MAINPATH=/src
CURLPATH=$MAINPATH/curlquic

QUICTLS=$CURLPATH/quictls
QUICTLSLIB=lib
NGHTTP3=$CURLPATH/nghttp3
NGTCP2=$CURLPATH/ngtcp2

apt-get install -y libtool libunwind-dev autoconf golang cargo dh-autoreconf libpsl-dev --no-install-recommends

# QUIC TLS
cd $MAINPATH
git clone --depth 1 -b openssl-3.1.4+quic https://github.com/quictls/openssl
cd openssl
./config enable-tls1_3 --prefix=$QUICTLS
make
make install

# Check if we are in a x64 architecture
if [ -d "$QUICTLS/lib64" ]; then
    QUICTLSLIB=lib64
fi

# NGHTTP3
cd $MAINPATH
git clone -b v1.1.0 https://github.com/ngtcp2/nghttp3
cd nghttp3
git submodule update --init
autoreconf -fi
./configure --prefix=$NGHTTP3 --enable-lib-only
make
make install

# NGTCP2
cd $MAINPATH
git clone -b v1.2.0 https://github.com/ngtcp2/ngtcp2
cd ngtcp2
autoreconf -fi
./configure PKG_CONFIG_PATH=$QUICTLS/$QUICTLSLIB/pkgconfig:$NGHTTP3/lib/pkgconfig LDFLAGS="-Wl,-rpath,$QUICTLS/$QUICTLSLIB" --prefix=$NGTCP2 --enable-lib-only
make
make install

# CURL
cd $MAINPATH
git clone https://github.com/curl/curl
cd curl
autoreconf -fi
LDFLAGS="-Wl,-rpath,$QUICTLS/$QUICTLSLIB" ./configure  --with-openssl=$QUICTLS --with-nghttp3=$NGHTTP3 --with-ngtcp2=$NGTCP2
make
make install

# Remove "Old version of libcurl4"
apt-get remove -y libcurl4t64

# Ubuntu image to test QUIC in GNS-3

## Description

This docker image is based on [Botzmeyer Lab](https://www.bortzmeyer.org/quic-demo.html).
It will contain the following main tools:
- [picoquic](ttps://github.com/private-octopus/picoquic)
- [curl](https://github.com/curl/curl/) with HTTP/3 support 

### Additional packages added
The packages added to the base image are visible in the ```Dockerfile```.

## Build locally to test 

To test if the image can be build without error, test locally on your machine with the command:
```
docker build -t quiclab .
```

## Force rebuild

If we change the content of the ```Dockerfile``` we have to add a new Tag to this repo. Then the CI/CD Pipeline will be executed.
Go to the "Repository --> Code --> Tag" menu to add the latest version number.

## How to use this image

- To download and use this image, just type:
    ```bash
    docker pull registry.forge.hefr.ch/bun/dockers/quiclab
    ```

- To run this image and if you would like to mount a local volume:
  ```bash
  docker run -it -v ~/mytmp:/mytmp registry.forge.hefr.ch/bun/dockers/quiclab bash
  ```
  This will map the directory ```~/mytmp``` to the directory ```mytmp``` inside the container.


## More informations
See Gitlab Docs:
* https://docs.gitlab.com/ee/user/packages/container_registry/


<hr>
(c) F. Buntschu 12.09.2024
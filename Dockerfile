FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /src

RUN set -x \
    && apt -y update \
    && apt -y install git cmake libssl-dev pkg-config build-essential openssh-server --no-install-recommends\
    && apt -y install dnsutils nano vim traceroute iproute2 iputils-ping tcpdump wget ca-certificates --no-install-recommends \
    && apt -y install git libtool libunwind-dev autoconf golang cargo dh-autoreconf libpsl-dev --no-install-recommends \
    && update-ca-certificates

RUN cd /src \
    && git clone https://github.com/h2o/picotls.git \
    && cd picotls \
    && git submodule init \
    && git submodule update \
    && cmake . \
    && make

RUN cd /src \
    && git clone https://github.com/private-octopus/picoquic.git \
    && cd picoquic \
    && cmake . \
    && make \
    && make install

COPY install_curl_http3.sh /src
RUN cd /src \
    && chmod +x install_curl_http3.sh \
    && ./install_curl_http3.sh

RUN set -x \
    && apt -y autoremove

CMD ["bash"]